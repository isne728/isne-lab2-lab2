#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.print();
	mylist.pushToTail('c');
	mylist.pushToTail('o');
	mylist.pushToTail('s');
	mylist.pushToTail('h');
	cout << "\n";
	mylist.print();
	cout << "\ntailpop is " << mylist.popTail();
	cout << endl;
	if (mylist.search('m')) {
		cout << "FOUND m";
	}
	else {
		cout << "NOT FOUND";
	}

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?

}