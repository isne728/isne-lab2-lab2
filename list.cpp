#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	tail = new Node(el, 0,tail);
	if (tail == 0) {
		tail = head;

	}
	else {
		tail->prev->next = tail; //tail equal to previous of next of tail
	}
	
	//TO DO!
}
char List::popHead()
{
	char el = head->data;
	Node* tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node* a = head; //create new pointer
	a = tail->prev; //a equal to previous of tail
	char save = tail->data; //save tail data to save
	delete tail;
	tail = a; //set tail point at a
	// TO DO!
	return save;
}
bool List::search(char el)
{
	Node* tmp = head; //create new pointer
	while (tmp != 0) {

		if (el == tmp->data) {
			return true;
		}
		tmp = tmp->next; //move b to next
	}

	// TO DO! (Function to return True or False depending if a character is in the list.
	return false; //if it not find data untill NULL it will return false
	
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node* tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}